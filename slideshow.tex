\documentclass{beamer}

\usepackage{algorithm}
\usepackage{algpseudocode}
% \usefonttheme[onlymath]{serif}

\usepackage{pgfpages}
\setbeameroption{show notes on second screen} 

\usetheme{tech}

\begin{document}
\title{Kitsune}
\subtitle{Un insieme di autoencoder \\ per rilevare intrusioni nelle reti}
\date{Gennaio 2020}
\author{Filippo Faldetta}
\institute{Universit\`a degli Studi di Firenze \\ Resilient and Secure Cyber Physical Systems}
%\logo{\includegraphics[scale=.25]{}}

\begin{frame}
	\maketitle
\end{frame}

\section{Introduzione}
\begin{frame}
	\begin{block}{\LARGE Introduzione}\end{block}
\end{frame}

\begin{frame}
	\frametitle{Introduzione}
	In questa presentazione vedremo un rilevatore di intrusioni, 
	basato su reti neurali artificiali, con le seguenti propriet\`a:
	\begin{itemize}
		\item \textbf{Elaborazione Online}: i dati vengono eliminati subito dopo 
		la loro analisi, in questo modo si mantiene basso l'utilizzo della 
		memoria.
		\item \textbf{Apprendimento Non Supervisionato}: l'addestramento delle 
		reti neurali non necessit\`a di dati precedentemente classificati.
		\item \textbf{Bassa Complessit\`a}: per mantenere la frequenza di analisi 
		dei pacchetti superiore a quella di arrivo degli stessi.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Introduzione}
	\includegraphics[scale=0.41]{pictures/kitsune/overview.png}
\note{
	\begin{enumerate}
		\item Le caratteristiche di un dato sono inviate ai neuroni di 
		input del primo livello di reti neurali.
		\item ogni \textit{autoencoder} tenta di ricostruire la 
		caratteristica del dato (che ha ricevuto) e computa l'errore 
		commesso nel farlo con lo Scarto Quadratico Medio (SQM).
		\item infine gli SQM sono inviati ad un \textit{autoencoder} 
		di output (secondo livello), il quale agisce come un votatore non lineare
		per il primo livello di \textit{autoencoder}.
	\end{enumerate}
}
\end{frame}

\section{Kitsnune NIDS}
\begin{frame}
	\begin{block}{\LARGE Kitsune NIDS}\end{block}
\end{frame}

\subsection{Struttura}
\begin{frame}
	\frametitle{Struttura}
	\includegraphics[width=\textwidth,height=3.5cm]{pictures/kitsune/structure.png}
\end{frame}
\note{
	\begin{itemize}
		\item \textbf{Catturatore di Pacchetti}: libreria esterna
			  responsabile per l'acquisizione dei pacchetti.
		\item \textbf{Parser di Pacchetti}: libreria esterna responsabile
			  del \textit{parsing} dei metadati dei pacchetti.
		\item \textbf{Estrattore di Caratteristiche (FE)}: componente
			  responsabile dell'estrazione di $n$ caratteristiche dal pacchetto
			  in arrivo per la creazione dell'istanza $\vec{x}\in\mathbb{R}^n$.
		\item \textbf{Mappatore di caratteristiche (FM)}: componente
			  responsabile per la creazione di un set di istanze pi\`u piccole (che
			  chiameremo \textbf{v}) da $\vec{x}$ e per l'apprendimento della
			  mappatura da $\vec{x}$ a \textbf{v}.
		\item \textbf{Rilevatore di Anomalie (AD)}: componente responsabile
			  il rilevamento di pacchetti anomali, data la rappresentazione
			  \textbf{v} del pacchetto.
	\end{itemize}
}

\begin{frame}
	\frametitle{Esempio: analisi di un pacchetto}
	\begin{enumerate}
		\item il \textbf{catturatore di pacchetti} acquisisce il pacchetto 
		grezzo.
		\item il \textbf{parser di pacchetti} analizza il pacchetto grezzo 
		e ne estrae informazioni.
		\item l'\textbf{estrattore di caratteristiche} ricava dai dati 
		ricevuti circa 100 statistche ($\vec{x}\in\mathbb{R}^n$) che descrivono lo stato del canale 
		di provenienza del pacchetto.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Esempio: analisi di un pacchetto}
	\begin{enumerate}
		\setcounter{enumi}{3}
		\item il \textbf{mappatore di caratteristiche} riceve $\vec{x}$ e \dots 
		\begin{itemize}
			\item Modalit\`a addestramento: \dots da questo apprende la mappatura 
			delle caratteristiche.
			\item Modalit\`a esecuzione: \dots mediante la mappatura appresa 
			genera un insieme di piccole istanze \textbf{v} da $\vec{x}$.
		\end{itemize}
		\item il \textbf{rilevatore di anomalie} riceve \textbf{v} e \dots
		\begin{itemize}
			\item Modalit\`a addestramento: \dots lo usa per addestrare il I 
			livello di autoencoder; lo scarto quadratico medio della 
			forward-propagation \`e usato per addestrare il II livello.
			\item Modalit\`a esecuzione: \dots analizza \textbf{v}; se lo 
			scarto quadratico medio del II livello supera un valore soglia, 
			allora viene sollevata un'allerta. 
		\end{itemize}
		\item il pacchetto originale, $\vec{x}$ e \textbf{v} vengono scartati.
	\end{enumerate}
\end{frame}

\subsection{Estrattore di caratteristiche}
\begin{frame}
	\frametitle{Estrattore di caratteristiche}
	Lo scopo di questa componente \`e quello di estrapolare le caratteristiche 
	che identificano il contesto di un pacchetto che transita per la rete.
	Per adempire a tale compito vengono usate delle statistiche incrementali 
	con decadimento.
\end{frame}

\begin{frame}
	\frametitle{Statistiche incrementali}

	Sia $S=\{x_1,\dots\}$ uno \textit{stream} di dati con $x_i \in \mathbb{R}$ e
	sia 
	$$IS:=(N,LS,SS)$$
	tale che  $N$, $LS$ e $SS$ siano rispettivamente numero, somma lineare 
	e somma dei quadrati delle istanze.

	Per inserire l'elemento $x_i$ in $IS$ basta fare 
	$$IS\leftarrow(N+1,LS+x_i,SS+x_i^2)$$
	
	In ogni momento, media, varianza e deviazione standard sono 
	$$\mu_S=\frac{LS}{N}\quad\sigma_S^2=|\frac{SS}{N}-\mu_S^2|
	\quad\sigma_S=\sqrt{\sigma_S^2}$$
\end{frame}

\begin{frame}
	\frametitle{Statistiche incrementali con decadimento}
	La funzione di decadimento \`e
	definita come 
	$$d_\lambda(t)=2^{-\lambda t}$$
	con $\lambda>0$ fattore di decadimento e $t$ il tempo trascorso 
	dall'ultima osservazione dallo \textit{stream} $S_i$.
	\skipline
	La tupla di una statistica incrementale con decadimento \`e quindi definita
	come 
	$$IS_{i,\lambda}:=(w,LS,SS,SR_{ij},T_{last})$$ 
	con $w$ peso attualmente utilizzato, $T_{last}$ \textit{timestamp} 
	dell'ultimo aggiornamento di $IS_{i,\lambda}$ e $SR_{ij}$ somma dei 
	prodotti residui tra gli stream $i$	e $j$ (usato per statistiche in 
	2D, come relazioni tra traffico in entrata e in uscita).
\end{frame}

\begin{frame}
	\frametitle{Statistiche incrementali con decadimento}
	L'aggiornamento del vettore diventa:

	\begin{algorithmic}
		\Procedure{update}{$IS_{i,\lambda},x_{cur},t_{cur},r_j $} 
			\State $\gamma\gets d_\lambda(t_{cur}-t_{last})$
				% \Comment{Calcolo fattore di decadimento}
			\State $IS_{i,\lambda}\gets(\gamma w, \gamma LS, \gamma SS,
			\gamma SR, T_{cur})$
				% \Comment{applico processo di decadimento}
			\State $IS_{i,\lambda}\gets(w+1,LS+x_{cur},SS+x_i^2,
			SR_{ij}+r_ir_j, T_{cur})$
				% \Comment{Inserimento}
			\State return $IS_{i,\lambda}$
		\EndProcedure
	\end{algorithmic}
\note{
	Commenti del codice:
	\begin{itemize}
		\item Calcolo fattore di decadimento
		\item applico processo di decadimento
		\item Inserimento
	\end{itemize}
}
\end{frame}

\subsection{Mappatore di caratteristiche}
\begin{frame}
	\frametitle{Mappatore di caratteristiche}
	Questa componente mappa le $n$ caratteristiche di $\vec{x}$ in $k$ 
	istanze pi\`u piccole, una per ogni autoencoder del I livello.

	Chiameremo l'insieme ordinato di $k$ istanze 
	$$\textbf{v}=\{\vec{v_1},\dots,\vec{v_k}\}$$

	Il raggruppamento che vedremo si basa sulla matrice della 
	covarianza $D$, calcolata incrementalmente, che viene usata 
	come una matrice di distanze. 
\end{frame}

\begin{frame}
	\frametitle{Mappatura delle caratteristiche}
	L'algoritmo che trova i k raggruppamenti :
	\begin{enumerate}
		\item inizia con $n$ gruppi, uno per punto di $D$
		\item cerca i due punti pi\`u vicini
		\item unisce i gruppi dei punti selezionati
		\item ripete i punti 2-3 fino a che non ottiene un solo 
		cluster contenente tutti gli $n$ punti 
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Mappatura delle caratteristiche}
	\begin{columns}
		\begin{column}{0.60\textwidth}
			\includegraphics[width=\textwidth]{pictures/kitsune/dendrogram.png}		
		\end{column}
		\begin{column}{0.40\textwidth}
			Per ottenere i $k$ gruppi, il dendrogramma viene scisso fino a 
			che tutti gli insiemi hanno dimensione minore o uguale a $m$.
		\end{column}
	\end{columns}
	\skipline
	Con $m$ nodi di input di un autoencoder del primo livello.
	\skipline
	I raggruppamenti superstiti descrivono la mappatura $f$.
\end{frame}

\subsection{Rilevatore di anomalie}
\begin{frame}
	\frametitle{Rilevatore di anomalie}
	Questa componente contiene una speciale rete neurale 
	chiamata KitNET formata da due livelli di autoencoder:
	\begin{itemize}
		\item \textbf{I livello} ($L^{(1)}$): insieme di $k$ autoencoder, ognuno \`e 
		mappato alla relativa istanza di \textbf{v}. Lo scarto quadratico medio 
		della ricostruzione del dato \`e inoltrato al livello successivo
		\item \textbf{II livello} ($L^{(2)}$): autoencoder che considera gli scarti 
		quadratici medi e le relazioni tra le  anormalit\`a dei sottospazi 
		del primo livello e genera un punteggio, agendo come 
		un votatore non lineare
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Inizializzazione}
	Ricevuto il primo set di istanze \textbf{v}, l'architettura di KitNET 
	\`e inizializzata a partire dalla struttura di \textbf{v} stesso.
	\skipline
	$L^{(1)}$ \`e definito come l'insieme ordinato $L^{(1)}=\{\theta_1
	\dots\theta_k\}$ tc ogni $\theta_i$ ha tre strati di neuroni: gli strati 
	esterni di $\textbf{dim}(\vec{v_i})$ neuroni e quello interno di 
	$\lceil\beta\cdot\textbf{dim}(\vec{v_i})\rceil$ neuroni, con 
	$\beta\in(0,1)$ che definisce la compressione.
	\skipline
	$L^{(2)}$ \`e definito come un singolo \textit{autoencoder} $\theta_0$, con $k$ 
	neuroni sia in \textit{input} che in \textit{output} e 
	$\lceil\beta\cdot k\rceil$ neuroni intermedi.
	\skipline
	$L^{(1)}$ e $L^{(2)}$ sono collegati mediante sinapsi senza pesi che 
	trasmettono gli scarti quadratici medi normalizzati. 
\end{frame}

\begin{frame}
	\frametitle{Modalit\`a addestramento}
	L'addestramento delle reti neurali \`e effettuato, come normale, 
	mediante backward propagation.
	\skipline
	Inoltre viene usata la discesa stocastica del gradiente utilizzando ogni 
	istanza di \textbf{v} esatamente una volta.
	\skipline
	Si noti che l'addestramento deve essere eseguito escusivamente su dati 
	normali.
\end{frame}

\begin{frame}
	\frametitle{Modalit\`a esecuzione}
	Durante l'esecuzione i paramentri interni della rete neurale 
	non vengono aggiornati, invece vengono calcolati gli scarti quadratici 
	medi della ricostruzione delle istanze delle caratteristiche dei dati.
	\skipline
	Gli scarti quadratici medi passano da $L^{(1)}$ a $L^{(2)}$ e quest'ultimo 
	misura l'anormalit\`a dell'instanza tenedo in considerazione le eventuali 
	relazioni che esistono tra le anormalit\`a delle diverse caratteristiche.
	\skipline
	Tale correlazione in modalit\`a di esecuzione pu\`o 
	essere rilevata se era presente anche del dati usati per l'addestramento. 
\end{frame}

\begin{frame}
	\frametitle{Punteggio anomalia}
	L'output che viene prodotto \`e uno scarto quadratico medio 
	$s\in[0,\infty)$, linearmente dipendente con l'anormalit\`a del 
	dato in analisi.
	\skipline
	Al fine utilizzare tale valore \`e prima necessario definire un valore 
	soglia $\phi$. 
	\skipline
	$\phi$ pu\`o essere definito o come il valore di anomalia maggiore trovato 
	durante l`addestramento o mediante il fit degli scarti quadratici medi con 
	una distribuzione lognormale o una non-standard (in questo caso l'allerta
	viene sollevata se $s$ ha una bassa probabilit\`a di occorrenza).
\end{frame}

\subsection{Complessit\`a}
\begin{frame}
	\frametitle{Complessit\`a}
	La complessit\`a di KitNET \`e $$O(km^2+k^2)=O(k^2)$$ 
	con $O(km^2)$ complessit\`a di $L^{(1)}$ 
	e $O(k^2)$ complessit\`a di $L^{(2)}$.
	\skipline
	La complessit\`a diventa $O(k^2)$ poich\`e $m$ (l'unico parametro in 
	input) \`e costante durante l'esecuzione. 
	\skipline
	Possiamo quindi identificare un caso ottimo quando $k=\frac{n}{m}$ 
	(con un miglioramento di un fattore $m$) 
	e uno pessimo quando $k=n$ (ovvero ogni caratteristica iniziale viene 
	analizzata da un'autoencoder diverso di $L^{(1)}$).
\end{frame}

\section{Valutazione}
\begin{frame}
	\begin{block}{\LARGE Valutazione}\end{block}
\end{frame}

\subsection{Dataset}
\begin{frame}
	\frametitle{Dataset}
	\begin{center}
		\includegraphics[scale=0.42]{pictures/kitsune/network.png}
	\end{center}
\note{
	\begin{itemize}
		\item \textbf{IoT} per aumentare il rumore
		\item Attacchi rete sorveglianza: 
			\begin{itemize}
				\item 1 Analisi
				\item 2 Man in the middle
				\item 3 DoS
			\end{itemize}
		\item Attacchi rete Iot:
			\begin{itemize}
				\item X Malware 
			\end{itemize}
	\end{itemize}
}
\end{frame}

\subsection{Metriche di valutazione}
\begin{frame}
	\frametitle{Metriche di valutazione}
	$TPR=\frac{TP}{TP+FN}$ e $FNR=\frac{FN}{FN+TP}$ sono usati quando 
	$\phi$ \`e scelto tale che il $FPR=\frac{FP}{FP+TN}$ \`e molto basso 
	(0.001).
	\skipline
	L'altra metrica usata (e che mostreremo in seguito) \`e l'$AUC$, 
	in quanto fornisce una panoramica sull'efficacia della tecnica in 
	analisi.
\end{frame}

\subsection{Performance nei rilevamenti}
\begin{frame}
	\frametitle{Performance nei rilevamenti}
	\begin{columns}
		\begin{column}{0.8\textwidth}
			\includegraphics[scale=0.52]{pictures/kitsune/AUC.png}
		\end{column}
		\begin{column}{0.2\textwidth}
			\includegraphics[scale=0.5]{pictures/kitsune/Legenda.png}
		\end{column}
	\end{columns}
	\note{
		\begin{itemize}
			\item in media risultati migliori
			\item 1 caso GMM supera Kit (Video Injection) di poco
			\item in base all'attacco la scelta di m decide i risultati.
			
			Futuro studio per dipendenza da m ?
		\end{itemize}	
	}
\end{frame}

\subsection{Performance dell'esecuzione}
\begin{frame}
	\frametitle{Performance dell'esecuzione}
	\includegraphics[width=\textwidth]{pictures/kitsune/density_processing.png}
	\note{
		Efficacia dell'utilizzo di pi\`u autoencoder:

		riduzione della varianza del tempo si esecuzione.
	}
\end{frame}

\setbeamertemplate{background}{\includegraphics[width=\paperwidth,height=\paperheight]{pictures/thats-all-folks.jpg}}
\begin{frame}[plain]
\end{frame}

\end{document}